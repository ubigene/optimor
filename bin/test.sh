#!/bin/bash

# Test runner script

source $(dirname $0)/env.sh

# activate virtualenv
source "$ENV_PATH/bin/activate"

# run python scripts
nosetests "$@"
