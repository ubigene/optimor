#!/bin/bash

# User interface runner script

source $(dirname $0)/env.sh

# activate virtualenv
source "$ENV_PATH/bin/activate"

# run command
"$@"