# -*- coding: utf-8 -*-
from py.o2crawler import O2Crawler, TASK_COUNTRIES
from py.o2crawler import UnknownCountryError, NoRatesError
from unittest import TestCase

TASK_COUNTRY_RATES = [
    u'£1.00', u'£1.00', u'£1.00',
    u'£1.20', u'£1.00', u'£1.00']


class O2CrawlerTest(TestCase):
    def test_country_list(self):
        with O2Crawler() as c:
            countries = c.get_countries()
            self.assertIsInstance(countries, dict)
            self.assertIn('Angola', countries)
            self.assertEqual(countries['Angola'], 'AGO')

    def test_interview_task(self):
        with O2Crawler() as c:
            for country, rate in zip(TASK_COUNTRIES, TASK_COUNTRY_RATES):
                pm_rates = c.get_pm_rates(country)
                self.assertEqual(pm_rates['landline'], rate)
                # c.random_wait()         # sleep to avoid detection

    def test_country_rates(self):
        with O2Crawler() as c:
            pm_rates = c.get_pm_rates('Ireland')
            self.assertDictEqual(
                {'landline': u'30p', 'mobiles': u'30p'},
                pm_rates)

    def test_country_not_exists(self):
        with O2Crawler() as c:
            self.assertRaises(UnknownCountryError, c.get_pm_rates, 'Lakeland')

    def test_country_no_rates(self):
        with O2Crawler() as c:
            self.assertRaises(
                NoRatesError, c.get_pm_rates, u'United Kingdom (UK)')
