# -*- coding: utf-8 -*-
from py.crawler import Crawler
from unittest import TestCase
import httplib
import os
import socket


def is_running(driver):
    """ Check Selenium driver status
    :param driver: Selenium driver object
    :returns: True if driver is open, otherwise False
    """
    try:
        driver.window_handles
        return True
    except (socket.error, httplib.CannotSendRequest):
        return False


class CrawlerTest(TestCase):
    def test_init(self):
        with Crawler() as c:
            c.driver.get("http://example.com/")
            self.assertEqual(u'Example Domain', c.driver.title)

    def test_disposal(self):
        with Crawler() as c:
            self.assertTrue(is_running(c.driver))
        self.assertFalse(is_running(c.driver))

    def test_disposal_headless(self):
        """ Test Xvfb instantiation and disposal
        """
        old_display = os.environ['DISPLAY']
        del(os.environ['DISPLAY'])
        with Crawler():
            self.assertIn('DISPLAY', os.environ)
        self.assertNotIn('DISPLAY', os.environ)
        os.environ['DISPLAY'] = old_display
