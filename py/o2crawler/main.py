# -*- coding: utf-8 -*-
""" O2 crawler user interface """
from pprint import pprint
import logging

from py.o2crawler.o2crawler import O2Crawler, UnknownCountryError, NoRatesError


TASK_COUNTRIES = [
    'Canada', 'Germany', 'Iceland',
    'Pakistan', 'Singapore', 'South Africa']

_log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def populate_arg_parser(parser):
    """ Argument parser specification for o2crawler
    """
    subparsers = parser.add_subparsers(help='Commands')

    get_country_codes = subparsers.add_parser(
        "get_country_codes",
        help="Print country codes")
    get_country_codes.set_defaults(run_o2crawler_mode_="get_country_codes")

    get_pm_rates = subparsers.add_parser(
        "get_pm_rates",
        help="Print rates for Pay Monthly customers")
    get_pm_rates.add_argument(
        "country", type=str,
        help="Country for which the rates will be returned")
    get_pm_rates.set_defaults(run_o2crawler_mode_="get_pm_rates")

    interview_task = subparsers.add_parser(
        "interview_task",
        help="""
        Print the price of calling a landline in the countries {}
         from an O2 Pay Monthly contract""".format(TASK_COUNTRIES))
    interview_task.set_defaults(run_o2crawler_mode_="interview_task")


def run(args, _):
    """ o2crawler command line interface implementation """
    def print_rate(country, rates):
        """ Print calling rates for the country to stdout """
        print u"\nCalling rates to {} for Pay Monthly customers".format(
            country)
        for name, value in rates.iteritems():
            print u"Calling to {} costs {} per minute".format(name, value)

    if args.run_o2crawler_mode_ == "get_country_codes":
        with O2Crawler() as crawler:
            countries = crawler.get_countries()
            pprint(countries)

    elif args.run_o2crawler_mode_ == "get_pm_rates":
        with O2Crawler() as crawler:
            try:
                pm_rates = crawler.get_pm_rates(args.country)
                print_rate(args.country, pm_rates)
            except (UnknownCountryError, NoRatesError) as err:
                _log.error(err.message)
                _log.error((
                    "For a list of valid countries use "
                    "run.sh o2crawler get_country_codes"))

    elif args.run_o2crawler_mode_ == "interview_task":
        with O2Crawler() as crawler:
            for country in TASK_COUNTRIES:
                try:
                    pm_rates = crawler.get_pm_rates(country)
                    del pm_rates['mobiles']
                    print_rate(country, pm_rates)
                    # crawler.random_wait()         # sleep to avoid detection
                except (UnknownCountryError, NoRatesError) as err:
                    _log.error(err.message)
                    _log.error((
                        "For a list of valid countries use "
                        "run.sh o2crawler get_country_codes"))
    else:
        raise ValueError(
            "Mode {} not supported".format(args.run_o2crawler_mode_))
