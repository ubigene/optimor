# -*- coding: utf-8 -*-
""" Implementation of a bespoke crawler for O2 website """
from py.crawler import Crawler
from selenium.common.exceptions import NoSuchElementException

try:
    import ujson as json
except ImportError:
    import json


class UnknownCountryError(KeyError):
    """ Raised when there is no such country
    in the supplier's database
    """
    pass


class NoRatesError(ValueError):
    """ Raised when a country does not have
    standard calling rates specified
    """
    pass


class O2Crawler(Crawler):
    """ A crawler for O2 international rates
    """
    countries_url = (
        'http://international.o2.co.uk/'
        'internationaltariffs/getallcountries')
    rates_url = (
        'http://international.o2.co.uk/'
        'internationaltariffs/getintlcallcosts?countryId={}')
    countries = None

    def get_countries(self):
        """ Get a list of supported countries with matching country codes
        :returns: a dictionary of (country name, country code) pairs
        """
        if not self.countries:
            self.driver.get(self.countries_url)
            response = json.loads(
                self.driver.find_element_by_tag_name("body").text)
            self.countries = {i['name']: i['countryId'] for i in response}
        return self.countries

    def _get_pm_rates_by_id(self, country_id):
        """ Get calling rates for Pay Monthly customers
        :param countryId: Country ID for which to get the rates
        :returns: a dictionary with rates for 'landline' and 'mobiles'
        """
        self.driver.get(self.rates_url.format(country_id))
        try:
            pm_rates_div = self.driver.find_element_by_xpath(
                '//div[@id="paymonthlyTariffPlan"]//div[@id="standardRates"]')
            rates = dict(
                landline=pm_rates_div.find_element_by_xpath(
                    './/td[preceding-sibling::td[.="Landline"]]').text,
                mobiles=pm_rates_div.find_element_by_xpath(
                    './/td[preceding-sibling::td[.="Mobiles"]]').text)
        except NoSuchElementException:
            raise NoRatesError(
                "No rates found for country: {}".format(country_id))
        return rates

    def get_pm_rates(self, country):
        """ Get calling rates for Pay Monthly customers
        :param country: Country name for which to get the rates
        :returns: a dictionary with rates for 'landline' and 'mobiles'
        """
        if not self.countries:
            self.get_countries()
        if country not in self.countries:
            raise UnknownCountryError("Unknown country: {}".format(country))
        return self._get_pm_rates_by_id(self.countries[country])
