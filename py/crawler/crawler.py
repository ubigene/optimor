# -*- coding: utf-8 -*-
""" Basic web crawler implementation """
from time import sleep
import os

from random import random
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from xvfbwrapper import Xvfb


# Setting up 'next generation FF WebDriver'
# see https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver
# for details
_ff_capabilities = DesiredCapabilities.FIREFOX  # pylint: disable=invalid-name
_ff_capabilities['marionette'] = True
_ff_capabilities["binary"] = os.environ['FF_BINARY']


class Crawler(object):
    """
    Defines a basic web crawler class using Selenium
    """
    def __init__(self):
        self.cleanup = []
        self.set_up()

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        for callback in self.cleanup:
            callback()

    def set_up(self):
        """ Create FF WebDriver and (optionally) Xvfb instance to run the driver
        """
        if 'DISPLAY' not in os.environ:
            screen_size = (1600, 1200)
            self.xvfb = Xvfb(*screen_size)
            self.add_cleanup(self.xvfb.stop)
            self.xvfb.start()
            self.driver = webdriver.Firefox(capabilities=_ff_capabilities)
            # self.driver.set_window_size(*screen_size)
        else:
            self.driver = webdriver.Firefox(capabilities=_ff_capabilities)
        self.driver.maximize_window()
        self.driver.implicitly_wait(.2)      # seconds
        self.add_cleanup(self.driver.quit)

    def add_cleanup(self, callback):
        """ Add callbacks to be run on disposal of the crawler
        :param callback: The function to run on disposal
        """
        self.cleanup.append(callback)

    @staticmethod
    def random_wait():
        """ Random wait for [0,1] seconds to avoid detection
        """
        sleep(random())
