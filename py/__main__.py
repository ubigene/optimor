""" This gets called by Bash bin scripts """
import sys
import os

PY_DIR = os.path.realpath(
    os.path.join(__file__, os.path.pardir, os.path.pardir))
sys.path.append(PY_DIR)

from py import main    # pylint: disable=wrong-import-position; path injection

if __name__ == "__main__":
    main(sys.argv[1:])
